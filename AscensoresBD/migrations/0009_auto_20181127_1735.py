# Generated by Django 2.1.2 on 2018-11-27 20:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('AscensoresBD', '0008_auto_20181127_1733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='solicitudtecnico',
            name='User',
        ),
        migrations.AddField(
            model_name='solicitudtecnico',
            name='uthor',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
