from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index.html'),
    path('menuTrabajador.html/',views.menuTrajabador, name = 'menuTrabajador.html'),
    path('quienesSomos.html/',views.quienesSomos, name = 'quienesSomos.html'),
    path('serviciosDisponibles.html/',views.serviciosDisponibles, name = 'serviciosDisponibles.html'),
    path('contacto.html/',views.contacto, name = 'contacto.html'),
    path('crearCliente.html/',views.formularioCliente, name = 'crearCliente.html'),
    path('listaClientes.html/',views.listaCliente, name = 'listaClientes.html'),
    path('solicitarTecnico.html/',views.formularioSolicitarTecnico, name = 'solicitarTecnico.html'),
    path('listaTrabajosDisponibles.html/',views.listaSolicitudes, name = 'listaTrabajosDisponibles.html'),
    path('menuAdmin.html/',views.vistaAdmin, name = 'menuAdmin.html'),
    path('registroReparacion.html/',views.vistaReparacion, name = 'registroReparacion.html'),
    path('register.html/', views.register, name = "register.html"),
    path('trabajosRealizados.html/',views.listaTrabajosRealizados, name = 'trabajosRealizados.html'),
]