# Generated by Django 2.1.2 on 2018-11-27 19:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('AscensoresBD', '0002_remove_cliente_ciudad_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cliente',
            old_name='comuna_id',
            new_name='comuna',
        ),
    ]
