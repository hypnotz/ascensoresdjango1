from django import forms
from .models import Cliente,SolicitudTecnico,Reparacion



class FormularioClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = [
            'nombre',
            'correo',
            'fono',
            'direccion',
            'comuna',
        ]

class FormularioSolicitud(forms.ModelForm):
     class Meta:
        model = SolicitudTecnico
        fields = [
            'id_cliente',
            'descripcion_problema',
            'tecnico',
            'estado',
        ]

class FormularioReparacion(forms.ModelForm):
    class Meta:
        model = Reparacion
        fields = [
            'cliente',
            'fecha',
            'hora_inicio',
            'serie_ascensor',
            'fallas_detectadas',
            'reparaciones_efectuadas',
            'piezas_cambiadas',
            'persona_presente',
            'ascensor',
            'hora_termino',
            'id_solicitud',
            'tecnico',
        ]


	