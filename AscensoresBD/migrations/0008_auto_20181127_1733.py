# Generated by Django 2.1.2 on 2018-11-27 20:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('AscensoresBD', '0007_auto_20181127_1727'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='solicitudtecnico',
            name='user',
        ),
        migrations.AddField(
            model_name='solicitudtecnico',
            name='User',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
