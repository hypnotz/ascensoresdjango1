# Generated by Django 2.1.2 on 2018-11-29 02:51

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('AscensoresBD', '0013_auto_20181127_1824'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reparacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('hora_inicio', models.DateTimeField()),
                ('serie_ascensor', models.CharField(max_length=50)),
                ('fallas_detectadas', models.TextField()),
                ('reparaciones_efectuadas', models.TextField()),
                ('piezas_cambiadas', models.TextField()),
                ('persona_presente', models.TextField()),
                ('hora_termino', models.DateTimeField()),
                ('ascensor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='AscensoresBD.Ascensor')),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AscensoresBD.Cliente')),
                ('id_solicitud', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='AscensoresBD.SolicitudTecnico')),
                ('tecnico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
