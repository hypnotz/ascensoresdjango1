from django.apps import AppConfig


class AscensoresbdConfig(AppConfig):
    name = 'AscensoresBD'
