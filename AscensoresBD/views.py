from django.shortcuts import render,redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import FormularioClienteForm,FormularioSolicitud,FormularioReparacion
from .models import Cliente,SolicitudTecnico
from django.contrib.auth import authenticate,login
from django.contrib.auth.forms import UserCreationForm





#-----------------INDEX---------------------------
def index(request):

    return render(request, 'AscensoresBD/index.html', {})

#-----------------CONTACTO---------------------------
def contacto(request):

    return render(request, 'AscensoresBD/contacto.html', {})

#-----------------MENU TRABAJADOR---------------------------
def menuTrajabador(request):

    return render(request, 'AscensoresBD/menuTrabajador.html', {})

#-----------------QUIENES SOMOS---------------------------

def quienesSomos(request):

    return render(request, 'AscensoresBD/quienesSomos.html', {})

#-----------------SERVICIOS DISPONIBLES---------------------------
def serviciosDisponibles(request):

    return render(request, 'AscensoresBD/serviciosDisponibles.html', {})

#-----------------TRABAJOS REALIZADOS---------------------------
def trabajosRealizados(request):

    return render(request, 'AscensoresBD/trabajosRealizados.html', {})
#-------------CREAR CLIENTE-------------------------------
def crearCliente(request):

    return render(request, 'AscensoresBD/crearCliente.html', {})



#---------------------Fomulario Cliente---------------------------

def formularioCliente(request):
    formCliente = FormularioClienteForm(request.POST or None) 
    if formCliente.is_valid():
        formCliente.save()
        return render(request,'AscensoresBD/crearCliente.html')
    context = {
        'formCliente':formCliente
        }
    return render(request, 'AscensoresBD/crearCliente.html',context)


#------------------LISTA CLIENTE--------------------------
def listaCliente(request):
    listaClientes = Cliente.objects.all()
    return render(request, 'AscensoresBD/listaClientes.html', {'listaClientes': listaClientes})

#--------------FORMULARIO SOLICITUD TECNICO-------------

def formularioSolicitarTecnico(request):
    formSolicitar = FormularioSolicitud(request.POST or None) 
    if formSolicitar.is_valid():
        formSolicitar.save()
        return render(request,'AscensoresBD/index.html')
    context = {
        'formSolicitar':formSolicitar
        }
    return render(request, 'AscensoresBD/solicitarTecnico.html',context)

#-----------LISTA TRABAJOS DISPONIBLES---------------------------

def listaSolicitudes(request):
    listaDisponible = SolicitudTecnico.objects.filter(estado=2)
    return render(request, 'AscensoresBD/listaTrabajosDisponibles.html', {'listaDisponible': listaDisponible})

#----------------------------VISTA MENU ADMINISTRADOR-------------------------------------------------
def vistaAdmin(request):

    return render(request, 'AscensoresBD/menuAdmin.html', {})
#-------------------------------REGISTRO REPARACION--------------------------------
def vistaReparacion(request):
    formReparacion = FormularioReparacion(request.POST or None) 
    if formReparacion.is_valid():
        formReparacion.save()
        return render(request,'AscensoresBD/menuTrabajador.html')
    context = {
        'formReparacion': formReparacion
        }
    return render(request, 'AscensoresBD/registroReparacion.html',context)

#---------------------VISTA REGISTRARSE-----------------------------
def register(request):
    if request.method == 'POST':
        formRegistroUsuario = UserCreationForm(request.POST)

        if formRegistroUsuario.is_valid():
            formRegistroUsuario.save()
            
            username = formRegistroUsuario.cleaned_data['username']
            password = formRegistroUsuario.cleaned_data['password1']

            user = authenticate(username=username, password=password)
            login(request, user)
            return render(request,'AscensoresBD/index.html/')
    else:
        formRegistroUsuario = UserCreationForm()
    context = {'formRegistroUsuario' : formRegistroUsuario}

    return render(request,'registration/register.html',context)

#------------LISTA TRABAJOS REALIZADOS---------------------------
def listaTrabajosRealizados(request):
    listaRealizados = SolicitudTecnico.objects.filter(estado=1)
    return render(request, 'AscensoresBD/trabajosRealizados.html', {'listaRealizados': listaRealizados})
