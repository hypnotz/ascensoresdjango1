from django.contrib import admin
from .models import Ciudad,Comuna,Cliente,SolicitudTecnico,EstadoOrden,Ascensor,Reparacion

admin.site.register(Ciudad)
admin.site.register(Comuna)
admin.site.register(Cliente)
admin.site.register(SolicitudTecnico)
admin.site.register(EstadoOrden)
admin.site.register(Ascensor)
admin.site.register(Reparacion)

