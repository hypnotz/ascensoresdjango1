from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Ciudad(models.Model):
	nombre = models.CharField(max_length=30)

	def __str__(self):
		return self.nombre

class Comuna(models.Model):
	nombre = models.CharField(max_length=30)
	ciudad_id = models.ForeignKey(Ciudad, on_delete=models.CASCADE)

	def __str__(self):
		return self.nombre

class Cliente(models.Model):
	nombre = models.CharField(max_length = 40)
	correo = models.EmailField(max_length=70, unique=True)
	fono = models.CharField(max_length=12, null=True, blank=True)
	direccion = models.CharField(max_length=50)
	comuna = models.ForeignKey(Comuna,on_delete=models.CASCADE)
	
	def __str__(self):
		return self.nombre

class Ascensor(models.Model):
	descripcion = models.CharField(max_length = 25)

	def __str__(self):
		return self.descripcion

class EstadoOrden(models.Model):
	descripcion = models.CharField(max_length = 25)

	def __str__(self):
		return self.descripcion

class SolicitudTecnico(models.Model):
	id_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
	descripcion_problema = models.CharField(max_length = 25)
	tecnico =  models.ForeignKey(User,on_delete=models.PROTECT,null=True,blank= True)
	estado  =  models.ForeignKey(EstadoOrden,on_delete=models.PROTECT,null=True,blank= True)
	
	def __str__(self):
		return self.descripcion_problema

class Reparacion(models.Model):
	cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
	fecha = models.DateField()
	hora_inicio = models.DateTimeField()
	serie_ascensor = models.CharField(max_length=50)
	fallas_detectadas = models.TextField()
	reparaciones_efectuadas = models.TextField()
	piezas_cambiadas = models.TextField()
	persona_presente = models.CharField(max_length=50)
	ascensor = models.ForeignKey(Ascensor,on_delete=models.PROTECT,null=True,blank= True)
	hora_termino = models.DateTimeField()
	id_solicitud= models.ForeignKey(SolicitudTecnico, on_delete=models.CASCADE)
	tecnico = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return self.fecha

	



	